import { ILesson } from './lessons'

export interface IModule {
  id: number
  name: string
  description: string
  next_module_id?: number
  lessons: ILesson[]
}

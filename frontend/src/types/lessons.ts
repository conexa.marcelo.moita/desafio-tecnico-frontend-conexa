export interface ILesson {
  id: number
  name: string
  description: string
  typeform_form_id: string
  next_lesson_id: number
  estimated_time_in_seconds: number
  is_done: boolean
}

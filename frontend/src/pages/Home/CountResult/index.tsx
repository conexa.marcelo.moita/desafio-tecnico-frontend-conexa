import { FunctionComponent } from 'react'
import { useCountContext } from '@/contexts/Count'
import useStyles from './styles'

const CountResult: FunctionComponent = () => {
  const { count } = useCountContext()
  const { countText } = useStyles()

  return <span css={countText}>{count}</span>
}

export default CountResult

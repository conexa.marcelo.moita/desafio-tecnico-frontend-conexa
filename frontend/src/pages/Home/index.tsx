import { FunctionComponent } from 'react'
// import Button from '@/components/Button'
import { Button } from '@conexasaude/hero'
import useStyles from './styles'
import { useCountContext } from '@/contexts/Count'
import CountResult from './CountResult'

const Home: FunctionComponent = () => {
  const { decrement, increment, reset } = useCountContext()
  const { container, resetButton } = useStyles()

  return (
    <div className="pageContainer">
      <h1>Hello</h1>
      <div css={container}>
        <Button size="lg" onClick={decrement}>
          -
        </Button>
        <CountResult />
        <Button onClick={increment}>+</Button>
        <Button onClick={reset} css={resetButton}>
          Reset
        </Button>
      </div>
    </div>
  )
}

export default Home

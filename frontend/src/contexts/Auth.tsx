import {
  createContext,
  useState,
  FunctionComponent,
  useContext,
  Dispatch,
  SetStateAction,
  useCallback,
} from 'react'

import { encode, decode } from 'js-base64'

import api from '@/services/api'

export interface IUser {
  firstName: string
  lastName: string
  email: string
  permissions?: string[]
}

type TokenType = string

interface SignInCredentials {
  email: string
  password: string
}

export interface IAuthContext {
  token?: TokenType
  user?: IUser
  setToken: Dispatch<SetStateAction<IAuthContext['token']>>
  setUser: Dispatch<SetStateAction<IAuthContext['user']>>
  signIn(credentials: SignInCredentials): Promise<void>
  signOut(): void
}

const AuthContext = createContext<IAuthContext>(null)

export const AuthProvider: FunctionComponent = ({ children }) => {
  const [user, setUser] = useState<IUser>(() => {
    const user = localStorage.getItem('@Conexa:user')

    return !user ? null : JSON.parse(decode(user))
  })

  const [token, setToken] = useState<TokenType>(() => {
    const token = localStorage.getItem('@Conexa:token') || null

    return token
  })

  const signIn = useCallback(async ({ email, password }) => {
    const response = await api.post('/login', { email, password })

    const { token, user } = response.data

    localStorage.setItem('@Conexa:token', token)
    localStorage.setItem('@Conexa:user', encode(JSON.stringify(user)))

    setToken(token)
    setUser(user)
  }, [])

  const signOut = useCallback(() => {
    localStorage.removeItem('@Conexa:token')
    localStorage.removeItem('@Conexa:user')

    setUser(null)
    setToken(null)
  }, [])

  return (
    <AuthContext.Provider
      value={{ user, token, setToken, setUser, signIn, signOut }}
    >
      {children}
    </AuthContext.Provider>
  )
}

export const useAuthContext = (): IAuthContext => {
  const context = useContext(AuthContext)
  if (!context) {
    throw new Error('useAuthContext must be used within a AuthProvider')
  }
  return context
}

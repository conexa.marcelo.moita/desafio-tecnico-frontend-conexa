import {
  createContext,
  useState,
  FunctionComponent,
  useContext,
  useCallback,
} from 'react'

export interface ICountContext {
  count: number
  increment(): void
  decrement(): void
  reset(): void
}

const CountContext = createContext<ICountContext>(null)

export const CountProvider: FunctionComponent = ({ children }) => {
  const [count, setCount] = useState(0)

  const increment = useCallback(() => setCount(prevState => prevState + 1), [])
  const decrement = useCallback(() => setCount(prevState => prevState - 1), [])
  const reset = useCallback(() => setCount(0), [])

  return (
    <CountContext.Provider value={{ count, increment, decrement, reset }}>
      {children}
    </CountContext.Provider>
  )
}

export const useCountContext = (): ICountContext => {
  const context = useContext(CountContext)
  if (!context) {
    throw new Error('useCountContext must be used within a CountProvider')
  }
  return context
}
